!(function (e) {
  'function' != typeof e.matches &&
    (e.matches =
      e.msMatchesSelector ||
      e.mozMatchesSelector ||
      e.webkitMatchesSelector ||
      function (e) {
        for (
          var t = this,
            o = (t.document || t.ownerDocument).querySelectorAll(e),
            n = 0;
          o[n] && o[n] !== t;

        )
          ++n;
        return Boolean(o[n]);
      }),
    'function' != typeof e.closest &&
      (e.closest = function (e) {
        for (var t = this; t && 1 === t.nodeType; ) {
          if (t.matches(e)) return t;
          t = t.parentNode;
        }
        return null;
      });
})(window.Element.prototype);

document.addEventListener('DOMContentLoaded', function () {
  const modalButtons = document.querySelectorAll('.js-open-modal');
  const overlay = document.querySelector('.js-overlay-modal');
  const closeButtons = document.querySelectorAll('.js-close-modal');
  const headerElem = document.querySelector('.header');

  modalButtons.forEach(function (item) {
    item.addEventListener('click', function (e) {
      e.preventDefault();
      const modalId = this.getAttribute('data-modal');
      const modalElem = document.querySelector(
        '.modal[data-modal="' + modalId + '"]'
      );
      modalElem.classList.add('active');
      if (modalId === 'menu') {
        headerElem.classList.add('active');
      } else {
        overlay.classList.add('active');
      }

      document.body.classList.add('stop-scroll');
    });
  });

  closeButtons.forEach(function (item) {
    item.addEventListener('click', function (e) {
      const modalId = this.getAttribute('data-modal');

      if (modalId !== 'menu') {
        const parentModal = this.closest('.modal');
        parentModal.classList.remove('active');
        overlay.classList.remove('active');
      } else {
        headerElem.classList.remove('active');
        const modalElem = document.querySelector(
          '.modal[data-modal="' + modalId + '"]'
        );
        modalElem.classList.remove('active');
      }
      document.body.classList.remove('stop-scroll');
    });
  }); // end foreach

  document.body.addEventListener(
    'keyup',
    function (e) {
      var key = e.keyCode;

      if (key == 27) {
        document.querySelector('.modal.active').classList.remove('active');
        document.querySelector('.overlay').classList.remove('active');
        document.body.classList.remove('stop-scroll');
      }
    },
    false
  );

  overlay.addEventListener('click', function () {
    document.querySelector('.modal.active').classList.remove('active');
    this.classList.remove('active');
    document.body.classList.remove('stop-scroll');
  });
});
