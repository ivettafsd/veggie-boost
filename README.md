# Veggie Boost

Landing page for the 'Veggieboost'
[Посилання на живу сторінку](https://ivetta-veggie.netlify.app/)

Всі матеріали до проєкту:
[Макет](https://www.figma.com/file/jGCCcfdvsxo3tYTdlApR1i/Veggie-boost?type=design&node-id=67-1193&t=0SH8TbuMUVDfwLy5-0)
[Технічне завдання](https://docs.google.com/spreadsheets/d/1u7j4nhdIWlEwBiohLVUmgAZUSh5L6uHBnXlnTQ0XROY/edit?usp=sharing)